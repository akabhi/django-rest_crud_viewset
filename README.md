# Django-REST CRUD_ViewSet

API CRUD operation using ViewSet

All the operations can be performed using ViewSet easily.
But only standard operation can be performed with ViewSet. Router is used in ViewSet instead of urls.
