
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from testapp import views

router = routers.DefaultRouter()
'''
in case of ModelViewSet base_name is optional
router.register('api', views.EmployeeCRUDCBV, base_name='api')
'''
router.register('api', views.EmployeeCRUDCBV)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
]
